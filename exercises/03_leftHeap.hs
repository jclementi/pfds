{-
* 3.1: Prove that the right spine of a leftist heap of size `n` contains at most `log(n+1)` elements.

Consider a leftist binary tree with a right spine having more than `d` elements, where `d` is `ceil(log(n+1))`. A balanced binary tree requires a depth of `d` to contain `n` elements. If the right spine has more than `d` elements, then there must be other spines with fewer than `d` elements, which contradicts the leftist requirement that right spines have rank no greater than their left siblings.

-}

data Heap a
  = E
  | N { rank :: Int
      , el   :: a
      , lh   :: Heap a
      , rh   :: Heap a
      }

makeNode :: a -> Heap a -> Heap a -> Heap a
makeNode x h1 h2
  | rank1 >= rank2 = N (rank2+1) x h1 h2
  | otherwise      = N (rank1+1) x h2 h1
  where
    rank1 = rank h1
    rank2 = rank h2

merge :: Ord a => Heap a -> Heap a -> Heap a
merge E h = h
merge h E = h
merge h1 h2
  | (el h1) > (el h2) = makeNode (el h2) (lh h2) $ merge (rh h2) h1
  | otherwise         = makeNode (el h1) (lh h2) $ merge (rh h1) h2

insert :: Ord a => a -> Heap a -> Heap a
insert a h = merge h $ N 1 a E E

findMin :: Heap a -> a
findMin E = error "min of empty tree"
findMin h = el h

deleteMin :: Heap a -> Heap a
deleteMin E = error "delete from empty tree"
deleteMin h = h

{-
* 3.2: Define insert directly rather than via a call to `merge`.
-}

directInsert :: Ord a => a -> Heap a -> Heap a
directInsert x E = N 1 x E E
directInsert x h
  | x < elem   = N 1 x h E
  | rlh == rrh = N (rank h) elem (directInsert x lheap) rheap
  | otherwise  = N (rank h) elem lheap (directInsert x rheap)
  where
    elem  = el h
    lheap = lh h
    rheap = rh h
    rlh   = rank lheap
    rrh   = rank rheap

{-
* 3.3: Implement a function `fromList :: [a] -> Heap a`. Create the heap from the list by creating singleton heaps from each element and merging them with `O(log n)` passes. Show that `fromList` takes only `O(n)` time.
-}

fromList :: Ord a => [a] -> Heap a
fromList [] = E
fromList as = mergeThings (map (\x -> N 1 x E E) as)
  where
    mergeThings (a:[]) = a
    mergeThings a      = mergeThings $ mergeThings_h a
    mergeThings_h (a:b:tail) = (merge a b) : mergeThings_h tail
    mergeThings_h (a:tail)   = [a]
    mergeThings_h []         = []
