import Control.Exception
{-
  2.1: Write a function `suffixes :: [a] -> [[a]]` that returns a list of all the suffixes of the list in decreasing order of length.

* Show that the resulting list can be generated in 0(n) time and represented in `O(n)` space.
 * Data structures are persistent. Nodes that are not changed do not need to be reconstructed. This lets us leave the entire original list unmodified. A list of suffixes requires only a single new node for each suffix. The new node's head will point to the member of the original list at which the suffix starts, and the new node's tail will point to the next node in the list of suffixes. This requires only `O(n)` new nodes where `n` is the number of elements in the list. Because only `n` new nodes are required, only 0(n) space is needed to store them.
-}
suffixes :: [a] -> [[a]]
suffixes [] = [[]]
suffixes ls = ls:suffixes(tail(ls))

{-
  2.2: In the worst case, `member` performs approximately `2*d` comparisons,s where `d` is the depth of the tree. Rewrite `member` to take no more than `d+1` comparisons by keeping track of a candidate element that _might_ be equal to the query element (say, the last element for which `<` has returned `false` or `<=` returned `true`) and checking for equality only when you hit the bottom of the tree.
-}
data Tree a = Node (Tree a) a (Tree a)
            | Empty
  deriving (Show)

member :: Ord a => a -> (Tree a) -> Bool
member x Empty = False
member x (Node lt y rt)
  | x <  y = member x lt
  | x >  y = member x rt
  | otherwise = True

member_n :: Ord a => a -> (Tree a) -> Bool
member_n quarry tree@(Node lt val rt) = member_nh val quarry tree

member_nh :: Ord a => a -> a -> (Tree a) -> Bool
member_nh candidate quarry Empty = candidate == quarry
member_nh candidate quarry (Node lt val rt)
  | quarry < val = member_nh candidate quarry lt
  | otherwise    = member_nh val       quarry rt

{-
  2.3: Inserting an existing element into a binary search tree copies the entire search path even though the copied nodes are indistiguishable from the originals. Rewrite insert using exceptions to avoid this copying. Establish only one handler per insertion rather than one handler per iteration.
-}
insert :: Ord a => a -> (Tree a) -> (Tree a)
insert x Empty = Node Empty x Empty
insert x tree@(Node lt y rt)
  | x < y  = Node (insert x lt) y rt
  | x > y  = Node lt y (insert x rt)
  | otherwise = tree

data TreeSetException = TreeSetException

insert_except :: Ord a => a -> Tree a -> Tree a
insert_except x tree =
  case insert_except' x tree of
    Right newtree -> newtree
    Left "duplicate" -> tree
  where insert_except' x Empty = Right $ Node Empty x Empty
        insert_except' x (Node lt y rt)
          | x < y     = Right $ Node (insert_except x lt) y rt
          | x > y     = Right $ Node lt y (insert_except x rt)
          | otherwise = Left "duplicate"

{-
  2.4: Combine the ideas of the previous two exercises to obtain a version of insert that performs no unnecessary copying and uses no more than `d+1` comparisons.
-}

{-
  2.5: Sharing can also be useful within a single object, not just between objects. For example, if the two subtrees of a given node are identical, then they cn be represented by the same tree.
    a: Using this idea, write a function `complete` of type `elem x -> int -> tree` where `complete(x, d)` creates a complete binary tree of depth d with x stored in every node.
    b: Extend this function to create balanced trees of arbitrary size. These trees will not always be complete binary trees, but should be as balanced as possible: for any given node, the two subtrees should differ in size by, at most, 1. This function should run in `O(log n)` time.
-}

-- returns a 'perfect' and 'full' binary tree of depth `d`
complete :: a -> Int -> Tree a
complete a d
  | d == 0    = Empty
  | otherwise = Node (complete a (d-1)) a (complete a (d-1))

-- returns a binary tree with `i` elements, as balanced as possible
complete_arb :: a -> Int -> Tree a
complete_arb a i
 | i == 0    = Empty
 | i == 1    = Node Empty a Empty
 | otherwise =
    let j = i-1
        half = div j 2
        remd = rem j 2
    in
      Node (complete_arb a half) a (complete_arb a (half + remd))

{-
  2.6: Adapt the `UnbalancedSet` functor to support finite maps rather than sets.
-}
data FiniteMapNode a = FiniteMapNode {key :: String, value :: a}
  deriving (Show, Eq, Ord)

{-
instance Eq (FiniteMapNode a) where
  one == two = (key one == key two)

instance Ord (FiniteMapNode a) where
  one <= two = (key one <= key two)
-}




