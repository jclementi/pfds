{-
* 3.4: Weight-biased leftist heaps are an alternative to leftist heaps that replace the leftist property with the 'weight-biased leftist property': the size of any left child is at least as large as the size of its right sibling.
 * a) Prove that the right spine of a weight-biased leftist heap contains at most `ceil(log(n+1))` elements.
  * Consider a weight-biased leftist heap with `(2^d)-1` elements, where `d` is the depth of the tree. A perfectly-balanced binary tree will have a right spine of length `ceil(log(n+1))` elements. If the right spine contains more elements, there must be other spines with fewer elements, violating the leftist property.
 * b) Modify the implementation of leftist heaps to obtain weight-biased leftist heaps.
-}

data Heap a
  = E
  | N { size :: Int
      , el   :: a
      , rh   :: Heap a
      , lh   :: Heap a
      }

instance Ord Heap where
  a <= b = (size a) <= (size b)

makeNode :: a -> Heap a -> Heap a -> Heap a
makeNode x h1 h2
  | size1 >= size2 = N (size2 + size1 + 1) x h1 h2
  | otherwise      = N (size2 + size1 + 1) x h2 h1
  where
    size1 = size h1
    size2 = size h2

merge :: Ord a => Heap a -> Heap a -> Heap a
merge E h = h
merge h E = h
merge h1 h2
  | (el h1) > (el h2) = makeNode (el h2) (lh h2) $ merge (rh h2) h1
  | otherwise         = makeNode (el h1) (lh h2) $ merge (rh h1) h2

insert :: Ord a => a -> Heap a -> Heap a
insert a h = merge h $ N 1 a E E

findMin :: Heap a -> a
findMin E = error "min of empty heap"
findMin h = el h

deleteMin :: Heap a -> Heap a
deleteMin E = error "delete from empty heap"
deleteMin h = h

{-
 * c) Currently, merge operates in two passes: a top-down pass consisting of calls to `merge`, and a bottom-up pass consisting of calls to the helper function `makeT`. Modify `merge` for weight-biased leftist heaps to operate in a single, top-down pass.
 * d) What advantages would the top-down version of `merge` have in a lazy environment? in a concurrent environment?
-}

tdMerge :: Ord a => Heap a -> Heap a -> Heap a
tdMerge E h = h
tdMerge h E = E
tdMerge h1 h2
  | el1 < el2 = N s3 el1 
  where
    s3 = (size h1) + (size h2)
    l3 = minimum 
