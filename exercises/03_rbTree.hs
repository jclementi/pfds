{-
* 3.8: Prove that the maximum depth of a node in a red-black tree of size `n` is at most `floor(log(n + 1))`.

* Things we know:
 * the longest possible path is no more than twice as long as the shortest possible path.
 * every path from root to an empty node contains the same number of black nodes.
 |
 * all paths from root to empty node have at least `l/2` steps, where `l` is the length of the longest path to an empty node. Alternatively, the longest path to an empty node cannot be longer than `2*s`, where `s` is the length of the shortest path.
 * because all paths to empty nodes have length of at least s, our tree is 'complete' to depth `s`. -> `s` = floor(log(n_s+1))` where `n_s` is the number of elements in the complete subtree of depth `s`.
 * `s = floor(log(n_s+1)) <= `floor(log(n+1))` because `s` comes from a subtree
 * longest can be no longer than `2*s = 2*floor(log(n_s+1)) <= 2*floor(log(n+1))`
  qed
-}

data Color = R | B
  deriving Eq

data Tree a
  = E
  | T { cl :: Color
      , el :: a
      , tl :: Tree a
      , tr :: Tree a
      }

member :: Ord a => a -> Tree a -> Bool
member x E = False
member x t
  | x < el t   = member x (tl t)
  | x > el t   = member x (tr t)
  | otherwise = True

insert :: Ord a =>  a -> Tree a -> Tree a
insert x t =
  let ins E = T R x E E
      ins s
        | x < el s  = balance $ T (cl s) (el s) (ins (tl s)) (tr s)
        | x > el s  = balance $ T (cl s) (el s) (tl s) (ins (tr s))
        | otherwise = s
      T _ y sl sr = ins t
  in
    T B y sl sr

balance :: Tree a -> Tree a
balance t
  | (cl t) == B
    && (cl (tl t)) == R
    && (cl (tl (tl t))) == R
      = T R
          (el t)
          (T B (el (tl t)) (tl (tl t)) (tr (tl t)))
          (T B (el (tl (tl t))) (tl (tl (tl t))) (tr (tl (tl t))))
  | (cl t) == B
    && (cl (tl t)) == R
    && (cl (tr (tl t))) == R
      = T R
          (el t)
          (T B (el (tl t)) (tl (tl t)) (tr (tl t)))
          (T B (el (tr (tl t))) (tr (tl (tl t))) (tr (tl (tl t))))
  | (cl t) == B
    && (cl (tr t)) == R
    && (cl (tl (tr t))) == R
      = T R
          (el t)
          (T B (el (tr t)) (tl (tr t)) (tr (tr t)))
          (T B (el (tl (tr t))) (tl (tl (tr t))) (tr (tl (tr t))))
  | (cl t) == B
    && (cl (tr t)) == R
    && (cl (tr (tr t)))== R
      = T R
          (el t)
          (T B (el (tr t)) (tl (tr t)) (tr (tr t)))
          (T B (el (tr (tr t))) (tl (tr (tr t))) (tr (tr (tr t))))
  | otherwise = t

