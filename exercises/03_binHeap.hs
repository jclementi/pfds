data BHeap a
  = N { rank :: Int
      , elmt :: a
      , subs :: [BHeap a]
      }

link :: Ord a => BHeap a -> BHeap a -> BHeap a
link t1 t2
  | elmt t1 <= elmt t2 = N (rank t1 + 1) (elmt t1) (t2 : (subs t1))
  | otherwise          = N (rank t2 + 1) (elmt t2) (t1 : (subs t2))

insHeap :: Ord a => BHeap a -> [BHeap a] -> [BHeap a]
insHeap a [] = [a]
insHeap a heaps@(h:t)
  | rank a < rank h = a : heaps
  | otherwise       = insHeap (link a h) t

merge :: Ord a => [BHeap a] -> [BHeap a] -> [BHeap a]
merge t [] = t
merge [] t = t
merge t1@(t1h:t1t) t2@(t2h:t2t)
  | rank t1h < rank t2h = t1h : merge t1t t2
  | rank t1h > rank t2h = t2h : merge t2t t1
  | otherwise           = insHeap (link t1h t2h) (merge t1t t2t)

removeMinHeap :: Ord a => [BHeap a] -> (BHeap a, [BHeap a])
removeMinHeap (h:[])  = (h, [])
removeMinHeap (h:t)
  | elmt h <= elmt min = (h, t)
  | otherwise          = (min, h : rest)
  where (min, rest) = removeMinHeap t

findMin :: Ord a => [BHeap a] -> a
findMin hs = elmt min
  where (min, _) = removeMinHeap hs

deleteMin :: Ord a => [BHeap a] -> [BHeap a]
deleteMin hs = merge (reverse (subs min)) rest
  where (min, rest) = removeMinHeap hs

{-
* 3.5: Define `findMin` directly rather than via a call to `removeMinTree`.
-}
directFindMin :: Ord a => [BHeap a] -> a
directFindMin hs@(h:t) = directFindMin' hs (elmt h)
  where
    directFindmin' [] min = min
    directFindMin' hs@(h:t) min
      | elmt h < min = directFindMin' t (elmt h)
      | otherwise    = directFindmin' t min

{-
* 3.6: Most of the rank annotations in this represntation of binomial heaps are redundant because we know that the children of a node of rank `r` have ranks `r-1, ... , 0`. Thus, we can remove the rank annotations from each node and instead pair each tree at the top-level with its rank.

  Reimplement binomial heaps with this new representation.

* This gets very complicated very quickly. Link should paste heaps of equal rank together, but none of them have rank information anymore. We have to calculate/find what ranks are present in the list by examining the binary representation of the rank stored in the head.
-}
-- rankless binomial heaps
type RlbHeap a = [(Int, [RlbTree a])]

data RlbTree a
  = RlbT { rlbElmt :: a
         , rlbSubs :: [RlbTree a]
         }

--rlLink :: Ord a => RlbHeap a -> RlbHeap a -> RlbHeap a

{-
* 3.7: One clear advantage of leftist heaps over binomial heaps is that `findMin` takes only `O(1)` time, rather than `O(log n)` time. The following functor skeleton improves the running time of `findMin` to `O(1)` by storing the minimum element separately from the rest of the heap.

  Complete this functor so that `findMin` takes `O(1)` time, and `insert`, `merge`, and `deleteMin` take `O(log n)` time.

* Solving this should just involve checking the minimum element to see if it needs to be swapped out before whatever operation is going on. If it's swapped during an insert, for example, the old minimum is inserted as normal into the heap.
-}



