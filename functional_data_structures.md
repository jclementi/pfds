functional data structures
==========================

# introduction

Most literature on data structures reference only imperative languages. Functional languages must do without destructive assignment, which is used liberally in most literature. Functional languages work with persistent data structures (which may have multiple versions persisting in memory), whereas imperative languages often use 'ephemeral' data structures (where one instance is destructively updated). Added to all this is the stigma and suggestion that functional languages have a certain lower bound on complexity which prevents them from matching the performance of imperative languages.

Reasoning about asymtotic complexity is difficult in a lazily evaluated programming language. To consider both the strictly-evaluated case (often the worst case) and the lazily-evaluated case (the 'amortized' case), we'll use standard ML with extensions for lazy evaluation.

Terms this book uses:
* abstraction: an abstract data type
* implementation: realization of an abstract data type
* object | version: an instance of a data type
* persistent identity: a unique identity that is invariant under updates. For example, the book refers to "the stack" even though there are many persistent versions of it as it gets updated.

The book will attempt to teach principles of functional data structure design that can be applied to particular cases.


# persistence

The persistence of functional data structures allows nodes unaffected by an update to be shared between old and new versions of the structure. How this copying and sharing occurs is important to understand.

## lists

When concatenating two lists, we cannot simply paste them together by modifying the last element of the first list. Modifyng that last element creates a new element, so each element of the first list must be updated to point to the new versions. This achieves persistence, but at the cost of `O(n)` work to copy nodes.

An update operation requires creating a new node at the index to be modified, so each node the code traverses on its way to the index must be copied.

## binary search trees

When traversing a tree for an insert or member operation, each node traversed is copied. The subtree that is not traversed is shared with the old version.


# familiar data structures

Leftist heaps, binomial queues, and red-black trees can get complicated in imperative languages, but functional implementations focus on the theory behind them and give us natural persistence without hellish pointer manipulation.

## leftist heaps

Also called 'min-heaps', they're often implemented as 'heap-ordered trees' in which the element at each node is no larger than the elements at each of its children. This ensures the minimum element is always at the root of the tree. 'Leftist' means the rank of any left child is at least as large as the rank of its right sibling, ensuring that the shortest path to an empty node is always down the right spine. Because the right spine has at most `O(n)` elements, the merge runs in `O(n)` time.

## binomial heaps

Binomial heaps are composed of binomial trees. A binomial tree of rank `r` is composed by taking two binomial trees of rank `r-1` and making one the leftmost child of the other. They can also be viewed as a collection of heap-ordered trees in which no two trees have the same rank. These are commonly represented as an element and a list of children, maintained in order of decreasing rank.

A binomial heap of `n` elements will contain at most `ceil(log(n+1))` trees, and the rank of those trees can be determined by looking at the binary representation of `n`. For example, the binomial heap with 21 (`10101`) elements is represented by a tree of rank 0, a tree of rank 4, and a tree of rank 16. Thinking about this binary representation gives the 'linking' operation an intuitive analog in binary addition. When an element is inserted into a binary tree, one node is added, so the representation of the heap changes in tandem with the binary representation of the number of thing in the heap. Any 'carry' in the binary addition is represented by a 'link' of two trees of rank `r` to create one of rank `r+1`.

## red-black trees

This popular form of balanced binary tree uses colored nodes and maintains two invariants to ensure balance:

* no red node has a red child
* every path from the root to an empty node contains the same number of black nodes

Note that empty nodes are considered to be black.

The `member` function can ignore color. The `insert` function must rebalance and recolor nodes. These changes propogate from the new leaf up to the root of the tree.


# lazy evaluation and streams

Lazy evaluation has two components: calculations are delayed until the result is needed, and that result is memoized so future references to it do not need to be computed. The rest of this section provides details on the `$` notation for lazy computation that the book uses.

A compiler will often memorize 'trivial' suspensions, which involve only a few variables or constructors and contain no function calls. This section is only going over implementation details of streams, which is what they call lazily-evaluated lists.


# fundamentals of amortization

Amortization is the practice of bounding the _entire_ operation rather than bounding individual iterations. Amortization may place an `O(n)` bound on an algorithm, but it allows some iterations to run in `O(log n)` or even `O(n)` time as long as the entire run is still bounded by `O(n)`. Proving an amortized bound usually involves demonstrating that the total amortized cost at any point during the algorithm is an upper bound on the total actual cost. The difference between them is called the 'accumulated savings'. Any operation that exceeds its amortized cost is 'expensive', and all others are 'cheap'. Proving bounds is done by demonstrating that expensive operations only occur when the accumulated savings are sufficient to cover the cost.

Analyzing amortized costs for using data structures is usually done with either the "banker's method" or the "physicist's method". The banker's method places 'credits', which represent savings, at different locations in the data structure. The amortized cost is the actual cost plus the credits allocated by the operation, minus the credits spent by the operation. The physicist's method defines a potential (as in 'electric potential') function that represents a lower bound on the accumulated savings. The two methods are, in theory, equivalent. However, the physicist's method is often simpler to work with than the banker's.

## queues

Functional queues are often implemented with two separate lists: one that holds the 'front' of the queue, and one that holds the 'back' of the queue, stored in reverse order. This allows us to access the head of the queue in `O(1)` time, and it also allows us to add to the end of the queue in `O(1)` time, since adding simply requires adding an element to the head of the back element. If the 'front' list is ever empty, the 'back' element is reversed, set to the 'front' element, and then set to empty. This means the front of the queue is always present and always accessible in `O(1)` time. In other terms, the front element can only be empty if the rear element is also empty.

The amortized analysis uses the physicist's method. We choose the pontential function to be the length of the rear list. Every enqueue operation takes one step (because adding to the rear is `O(1)`) and it increases the potential function (the length of the rear list) by 1. The total amortized cost of this operation is 2. Every 'tail' that does not reverse the rear list takes one actual step and it leaves the potential unchanged, so the amortized cost is 1. Finally, every 'tail' that does reverse the rear list takes `m+1` actual steps and sets the new rear list to `empty`, which decreases the potential by `m`, for a total amortized cost of 1. This means we get a constant amortized cost even though some operations are worst-case `O(n)`.

## binomial heaps

We already know that insert on binomial heaps runs in `O(log(n))` time, but we can now show that it runs in `O(1)` amortized time.

Define the potential to be the number of trees in the heap. An insert operation takes `k+1` steps, where `k` is the number of links it has to perform. If there were initially `t` trees, there are `t-k+1` trees after the insertion (every link removes a tree). Thus, the change in potential is 2, which is constant.

## splay heaps

Splay heaps are a very successful amortized data structure. Every operation on a splay heap also performs some transformations that tend to increase balance in the tree. Even queries transform the tree. As a result, even though some operations may take `O(n)`, the total amortized cost is `O(log(n))`.

Inserting into a splay tree involves partitioning the existing tree into 'bigger' and 'smaller' subtrees, then inserting the element at the root. This is analogous to the 'partition' in quicksort. This doesn't seem very balanced on its own, so we add the hueristic that any time we follow two left branches in a row, we rotate those two nodes. This hueristic results in restructuring the tree such that the search path we take is always shortened if the tree is unbalanced.

Finding and deleting the minimum element always takes us to the very end of the left branches of the tree. There's no need for comparisons thanks to the structure.

Splay trees are the fastest known implementation of heaps for most applications that do not depend on persistence and do not need a fast "merge" operation (this takes `O(n)` time for splay trees).

## pairing heaps

Pairing heaps are heap-ordered multi-way trees. The heap-ordering makes finding the min element trivial. Merge and insert work as you'd expect as well. Insert creates a singleton tree and merges. The pairing heaps get their name from the `deleteMin` operation, which discards the root element, then merges the children in two passes. The first pass merges children in pairs from left to right, and the second pass memrges the resulting trees from right to left.

`findMin`, `insert`, and `merge` all run in `O(1)` in the worst case. However, delete min can take `O(n)` in the worst case. Using the same strategy as we did with splay heaps, we can show that all of these run in `O(log(n))` amortized time. There's a conjecture that they actually run in `O(1)` amortized time, but that hasn't been proven yet.

Pairing heaps are nearly as fast as splay heaps in applications that don't require merging, and they're much faster in applications that do. However, they, like splay heaps, should not be used for applications that do not take advantage of persistence.

## bad stuff

The analysis in this chapter assumes that the data structures are all used ephemerally (without persistence). If persistence _is_ used, then the amortized time tends to fall apart. Rebuilding the queue when taking the tail actually takes `O(n^2)` time, and everything stinks.

The analysis breaks down because of the assumptions made by the banker and physicist's methods. The banker's method assumes that no credit is spent more than once, and the physicist requires that the output of one operation be the input of the next (that no output is used more than once). This problem can be solved with lazy evaluation.
